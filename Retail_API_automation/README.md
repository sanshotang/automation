This automation framework works in this fashion that you make a new module(or any other you prefer) under the 
parent project and utilize the utilities provided to you in the base automation project. Please refer to the below 
section to add your app testcases.

Adding a Module in Framework:
=============================


1. Also add your module corresponding to the app to be tested in the parent pom in the modules tag as follows(Replace
 your_app_name)
 
    ```XML
    <modules>
        <module>your_app_name</module>
        <module>base-automation</module>
    </modules>
    ```    
2. Add profile in main pom.xml file

    ```XML
    <profile>
        <id>{$PROFILE_NAME}</id>
        <properties>
            <test.app.name>{Give here the folder name inside testngxml folder where test suite lies}</test.app.name>
            <test.suite.name>{Give here the testng suite xml file name}</test.suite.name>
        </properties>
    </profile>
    ```        
3. Add you Module as child module to the framework and add dependency to the base automation

    ```XML
    <dependency>
        <artifactId>base-automation</artifactId>
        <groupId>retail-automation</groupId>
        <version>1.0</version>
    </dependency>
    ```
4. Also in your module pom add the dependency of the failsafe plugin and also the way you want to use, like for
example, one sample implementation is as follows

    ```XML
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>2.17</version>
        <executions>
            <execution>
                <id>integration-test</id>
                <configuration>
                    <suiteXmlFiles>
                        <suiteXmlFile>testngxml/${test.app.name}/${test.suite.name}</suiteXmlFile>
                    </suiteXmlFiles>
                    <skip>false</skip>
                </configuration>
                <goals>
                    <goal>integration-test</goal>
                </goals>
            </execution>
            <execution>
                <id>verify</id>
                <configuration>
                    <skip>false</skip>
                </configuration>
                <goals>
                    <goal>verify</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    ```
    Here if you are managing multiple apps through a single module than the above mentioned structure will be very much
    useful here you can specify your app/suite name at the run time via mvn command line options as mentioned below(This
    can be very useful in creating jenkin jobs too).

5. To run the testcases of particular module through command line:

    ```Shell
    mvn clean verify -P{$PROFILE_NAME}
    ```
    e.g.
     
    ```Shell
    mvn clean verify -Pcrawler
    ```
    Though it is always preferred that you compile only your module via the command line mentioned below with that it
     will pick the dependent modules too and will build it. So it will skip compiling the other apps and hence won't
     create problems. For ex- if one projects has thrift as a dependency then you can run your project without
     setting up thrift. Below mentioned is the command line.
     
     ```Shell
     mvn clean verify -pl <appName> -am -P<profileName>
     ```
6. To run specific group of testcases:
    
    ```Shell
    mvn clean verify -P{$PROFILE_NAME} -Dgroups={$GROUP_NAME}
    ```
    e.g. 
    
    ```Shell
    mvn clean verify -Pcrawler -Dgroups=@Smoke