package retail.app.api.AutoMobiles;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.json.JSONException;
import retail.app.util.HeaderFactory;
import retail.app.util.PropHelper;

import java.io.IOException;
import java.util.HashMap;


/**
 * Created by Srikanth.k@shotang.com on 21/08/2017
 */

public class CarsAndBikesClients {
    private static String testStage;
    public CarsAndBikesClients() throws JSONException, IOException {
        PropHelper helper=null;
        try {
            helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Automobiles_Analyticshome");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This method is used for Getting the Categories
    public static Response getCategories() throws Exception {
        String url = testStage + "/api/v1/category/v3/categories";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeaderforAutomobiles();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Automobiles Info
    public static Response getAutomobilesAdvancedFilterData() throws Exception {
        String url = testStage + "/api/v1/category/v3/cards/0/getAutomobileInfo";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeaderforAutomobiles();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Automobiles Cars And Bike Cards
    public static Response getAutomobilesCardsDetails(String category_id) throws Exception {
        String url = testStage + "/api/v1/category/v3/cards/0/getAllCards?categoryId=" + category_id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeaderforAutomobiles();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Automobiles Cars And Bike Cards
    public static Response getAutomobilesCarsAndBikesCardsProductDetails(String entity_id) throws Exception {
        String url = testStage + "/api/v1/product/v3/product?page_size=20&category_id=173&page=1&entity_type=bike_seat_covers&entity_id=" + entity_id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeaderforAutomobiles();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }


}