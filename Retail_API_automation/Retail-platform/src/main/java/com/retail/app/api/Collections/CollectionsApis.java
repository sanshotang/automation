package retail.app.api.Collections;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import retail.app.util.PropHelper;
import retail.app.util.HeaderFactory;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

//import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

//import static sun.security.x509.X509CertInfo.KEY;

/**
 * Created by Srikanth.k@shotang.com on 03/07/2017
 */
public class CollectionsApis {
    private static String testStage;
    public CollectionsApis() throws JSONException {
        try {
            PropHelper helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This method is used for Getting the Cards For All Categories

    public static Response getProductsForCollections(String entity_Id) throws Exception {
        String url = testStage + "/api/v1/product/v3/product?category_id=59&entity_type=collections&entity_id=" + entity_Id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

}



