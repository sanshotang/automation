package retail.app.util;

import java.util.HashMap;

/**
 * User: Santhosh Kumar GA
 * To change this template use File | Settings | File Templates.
 */
public class HeaderFactory {

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON = "multipart/form-data";
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    public static final String IPADDRESS = "127.0.0.1";
    public static final String AUTHORISATION = "Authorization";
    public static final String STAGING_AUTHVALUE = "Bearer b95a49e39f7938683a4813e7db7b500a6ac5cc32-id-3033";
    public static final String IROBOT_AUTHVALUE= "Bearer b95a49e39f7938683a4813e7db7b500a6ac5cc32-id-3033";
    public static final String AUTOMOBILES_TEST9_AUTHVALUE= "Bearer 717a3191f09e0d85173233a15e9d2b22beef129d-id-14793";
    public static final String IROBOT_BULK_REQUEST_USER_AUTHVALUE= "Bearer 9b4e8c82e0450c0da74b319b66e46ffa18b65f22-id-6897";

    public static final String X_Request_ID = "String";
    public static final String COOKIE = "Cookie";
    public static final String COOKIE_VALUE = "PHPSESSID=h3ta68mdler33dv1f5g1kbsl52";


    public static HashMap<String, String> defaultHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(AUTHORISATION, IROBOT_AUTHVALUE);
        //headers.put(CONTENT_TYPE, APPLICATION_JSON);
        headers.put(COOKIE, COOKIE_VALUE);
        return headers;
    }

    public static HashMap<String, String> defaultHeaderforAutomobiles() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(AUTHORISATION, AUTOMOBILES_TEST9_AUTHVALUE);
        //headers.put(CONTENT_TYPE, APPLICATION_JSON);
        headers.put(COOKIE, COOKIE_VALUE);
        return headers;
    }

    public static HashMap<String, String> defaultHeaderForbulkRequestUser() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(AUTHORISATION, IROBOT_BULK_REQUEST_USER_AUTHVALUE);
        //headers.put(CONTENT_TYPE, APPLICATION_JSON);
        headers.put(COOKIE, COOKIE_VALUE);
        return headers;
    }

}
