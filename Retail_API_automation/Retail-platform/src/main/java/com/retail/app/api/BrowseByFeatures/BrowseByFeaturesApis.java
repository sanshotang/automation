package retail.app.api.BrowseByFeatures;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.json.JSONException;
import retail.app.util.PropHelper;
import retail.app.util.HeaderFactory;
import java.io.IOException;
import java.util.HashMap;

//import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

//import static sun.security.x509.X509CertInfo.KEY;

/**
 * Created by Srikanth.k@shotang.com on 06/07/2017
 */
public class BrowseByFeaturesApis {
    private static String testStage,pricefilters,FourGPhones,dualsims,ram , battery, screensize;
    public BrowseByFeaturesApis() throws JSONException, IOException {
        PropHelper helper=null;
        try {
            helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");

        } catch (IOException e) {
            e.printStackTrace();
        }
        pricefilters = helper.priceFilters();
        FourGPhones = helper.fourGPhonesFilters();
        dualsims = helper.dualSimsFilters();
        ram = helper.ramFilters();
        battery = helper.batteryFilters();
        screensize = helper.screenSizeFilters();

    }

    // This method is used for Getting the Browse By Features results

    public static Response getProductsForBrowseByFeatures(String entity_Id) throws Exception {
        String url = testStage + "/api/v1/product/v3/product?category_id=59&entity_type=features&page_size=20&entity_id=" + entity_Id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on price filters

    public static Response applyPriceFilter() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(pricefilters).post(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on 4G Phones filters

    public static Response applyFourGPhonesFilters() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(FourGPhones).post(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on Dual Sims filters

    public static Response applyDualSimsFilters() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(dualsims).post(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on Ram filters

    public static Response applyRamFilters() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(ram).post(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on Battery filters

    public static Response applyBatteryFilters() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(battery).post(url);
        return response;
    }

    // This method is used for Getting the Browse By Features results based on Screen Size filters

    public static Response applyScreenSizeFilters() throws Exception {
        String url = testStage + "/api/v1/search/v3/search/0/getProducts";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(screensize).post(url);
        return response;
    }
}



