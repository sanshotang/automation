package retail.app.api.Offers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import retail.app.util.PropHelper;
import retail.app.util.HeaderFactory;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

//import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

//import static sun.security.x509.X509CertInfo.KEY;

/**
 * Created by Srikanth.k@shotang.com on 11/07/2017
 */
public class OffersApis {
    private static String testStage,favs, getHintData , placeBulkOrderRequest;
    public OffersApis() throws JSONException, IOException {
        try {
          PropHelper  helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");
            getHintData=helper.getHintData();
            placeBulkOrderRequest = helper.placeBulkOrderRequest();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This method is used for Getting the Products For Offer Type as Cashback

    public static Response getProductsForOfferTypeAsCashback(String cat_Id) throws Exception {
        String url = testStage + "/api/v1/product/v2/product?entity_type=offer&entity_id=1&category_id="+ cat_Id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }


    // This method is used for Getting the Products For Offer Type as Bundle Offer

    public static Response getProductsForOfferTypeAsBundleOffer(String cat_Id) throws Exception {
        String url = testStage + "/api/v1/product/v2/product?entity_type=offer&entity_id=9&category_id="+ cat_Id ;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Products For Group deal Offer

    public static Response getGroupDeals() throws Exception {
        String url = testStage + "/api/v1/offers/v2/group_deals";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Reverse Auction Landing Page

    public static Response getBulkOrderCard() throws Exception {
        String url = testStage + "/api/v1/auction/reverse_auction/landing";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Reverse Auction Terms And Condition Page

    public static Response getBulkOrdersTermsPage() throws Exception {
        String url = testStage + "/api/v1/auction/reverse_auction/terms";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Reverse Auction Misc data

    public static Response getBulkOrdersMiscData() throws Exception {
        String url = testStage + "/api/v1/auction/reverse_auction/misc";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Reverse Auction Nokia 105 product Data

    public static Response getBulkOrdersNokia105ProductData() throws Exception {
        String url = testStage + "/api/v1/auction/reverse_auction/getProductOptions?product_id=74";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Product Hint data like best price and best min qty

    public static Response getHintData() throws Exception{
        String url = testStage + "/api/v1/auction/reverse_auction/getHintData";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).body(getHintData).post(url);
        return response;
    }

    // This method is used for Placing  the Bulk order request

    public static Response placeBulkOrderRequest() throws Exception{
        String url = testStage + "/api/v1/auction/reverse_auction/add";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeaderForbulkRequestUser();
        Response response= RestAssured.given().headers(headers).body(placeBulkOrderRequest).post(url);
        return response;
    }
}