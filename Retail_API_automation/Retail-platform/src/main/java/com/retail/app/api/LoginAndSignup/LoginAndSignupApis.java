package retail.app.api.LoginAndSignup;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import org.json.JSONException;
import retail.app.util.HeaderFactory;
import retail.app.util.PropHelper;

import java.io.IOException;
import java.util.HashMap;

//import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

//import static sun.security.x509.X509CertInfo.KEY;

/**
 * Created by Srikanth.k@shotang.com on 04/08/2017
 */
public class LoginAndSignupApis {

    private static String testStage, Signup;
    public LoginAndSignupApis() throws JSONException, IOException {
        PropHelper helper=null;
        try {
          helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // This method is used for verifying retailer Login

    public static com.squareup.okhttp.Response verifyLoginFlow(String email_id, String password) throws Exception {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
        RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n"+email_id+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n"+password+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
        Request request = new Request.Builder()
                .url("https://staging.shotang.com/api/v1/oauth2/token")
                .post(body)
                .addHeader("accept", "application/json")
                .addHeader("authorization", "Basic cmVzdDpyZXN0ZnVsd2Vic2VydmljZWFwaWZvcnNob3Rhbmc")
                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "7dea9dd2-ffa9-e733-3206-18846b0624c7")
                .build();

        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response;
    }

    // This method is used for verifying Retailer Signup

    public static Response verifySignupFlow() throws Exception {
        String url = testStage + "api/v1//account/register";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(Signup).post(url);
        return response;
    }

}