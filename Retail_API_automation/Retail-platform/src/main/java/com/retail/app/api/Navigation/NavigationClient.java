package retail.app.api.Navigation;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import retail.app.util.PropHelper;
import retail.app.util.HeaderFactory;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

//import com.sun.org.apache.xml.internal.security.utils.HelperNodeList;

//import static sun.security.x509.X509CertInfo.KEY;

/**
 * Created by Santhosh.kumar@shotang.com on 20/06/2017
 */
public class NavigationClient {
    private static String testStage,favs;
    public NavigationClient() throws JSONException, IOException {
        PropHelper helper=null;
        try {
            helper=new PropHelper();
            testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");

        } catch (IOException e) {
            e.printStackTrace();
        }
        favs=helper.readFavourites();
    }

    // This method is used for Getting the Categories

    public static Response getCategories() throws Exception {
        String url = testStage + "/api/v1/category/v2/categories";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Products For Accessories SubCategory

    public static Response getProductsForAccessoriesSubCategory() throws Exception {
        String url = testStage + "/api/v1/product/v2/product?category_id=121&entity_type=accessory&entity_id=120&offset=0&limit=40";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Cards For All Categories

    public static Response getCardsForMobileCategory(String cat_Id) throws Exception {
        String url = testStage + "/api/v1/category/v2/cards/" + cat_Id;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Products For Nokia Brand
    public static Response getProductsForNokiaBrand() throws Exception {
        String url = testStage + "/api/v1/product/v3/product?category_id=59&entity_type=brand&entity_id=11";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Getting the Nokia 105 Ds Data
    public static Response getProductDetails(String productID) throws Exception {
        String url = testStage + "/api/v1/product/v3/product/"+productID;
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).get(url);
        return response;
    }

    // This method is used for Posting Favourite data
    public static Response postFavourite(String entityID, String catID) throws Exception {
        String url = testStage + "/api/v1/favourites/v2/favourites";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).body(favs).post(url);
        return response;
    }

    // This method is used for Deleting Favourite data
    public static Response deleteFavs(String entityID, String catID)  throws Exception{
        String url = testStage + "/api/v1/favourites/v2/favourites?category_id=59&entity_id=25&entity=brand";
        System.out.println("URL :: " + url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response = RestAssured.given().headers(headers).delete(url);
        return response;
    }
}