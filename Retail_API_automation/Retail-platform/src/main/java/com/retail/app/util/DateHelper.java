package retail.app.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: harshavardhan.p
 * Date: 23/03/15
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class DateHelper {

    private SimpleDateFormat simpleDateFormat;
    private Date date;
    private Calendar calendar;
    private Long unixTime;
    private String returnDate;

    public String setCurrentDateInJodaFormat(){

        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
        this.date = new Date();
        this.calendar = Calendar.getInstance();
        this.calendar.setTime(this.date);
        this.date = this.calendar.getTime();
        return this.simpleDateFormat.format(this.date);

    }

    public Long setCurrentDatePlus2MinInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 2);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus1MinInEpochFormat(){

//        returnDate = DateTime.now().plusMinutes(1).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 1);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus6MinInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 6);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus20MinInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 20);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus40MinInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 40);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus61MinInEpochFormat(){

//        returnDate = DateTime.now().plusMinutes(61).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 61);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus66MinInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 66);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus50MinInEpochFormat(){

//        returnDate = DateTime.now().plusMinutes(50).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 50);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus90MinInEpochFormat(){

//        returnDate = DateTime.now().plusMinutes(90).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, 90);
        return c.getTime().getTime();
    }

    public long setCurrentDatePlus6DaysInEpochFormat(){

//        returnDate = DateTime.now().plusDays(6).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 6);
        return c.getTime().getTime();
    }

    public Long setCurrentDatePlus8DaysInEpochFormat(){

//        returnDate = DateTime.now().plusDays(8).toDateTimeISO().toString();
//        return returnDate;
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 8);
        return c.getTime().getTime();
    }

    public Long setCurrentDatePlus9DaysInEpochFormat(){

//        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//        this.date = new Date();
//        this.calendar = Calendar.getInstance();
//        this.calendar.setTime(this.date);
//        this.calendar.add(5, 9);
//        this.date = this.calendar.getTime();
//        return this.simpleDateFormat.format(this.date);
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 9);
        return c.getTime().getTime();
    }


    public long setCurrentDatePlus10DaysInEpochFormat(){

//        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//        this.date = new Date();
//        this.calendar = Calendar.getInstance();
//        this.calendar.setTime(this.date);
//        this.calendar.add(5, 10);
//        this.date = this.calendar.getTime();
//        return this.simpleDateFormat.format(this.date);
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 10);
        return c.getTime().getTime();
    }

    public long setCurrentDateMinus5DaysInEpochFormat(){

//        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//        this.date = new Date();
//        this.calendar = Calendar.getInstance();
//        this.calendar.setTime(this.date);
//        this.calendar.add(5, -5);
//        this.date = this.calendar.getTime();
//        return this.simpleDateFormat.format(this.date);
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -5);
        return c.getTime().getTime();
    }

    public long setCurrentDateMinus10DaysInEpochFormat() {

//        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//        this.date = new Date();
//        this.calendar = Calendar.getInstance();
//        this.calendar.setTime(this.date);
//        this.calendar.add(5, -10);
//        this.date = this.calendar.getTime();
//        return this.simpleDateFormat.format(this.date);
//        this.unixTime = System.currentTimeMillis() + 10 * 24 * 60 * 60 * 1000;
//        return Long.valueOf(this.unixTime);

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -10);
        return c.getTime().getTime();
    }

    public Long setCurrentDateInEpochFormat(){

        this.unixTime = System.currentTimeMillis();
        return Long.valueOf(this.unixTime);
    }

    public Long setCurrentDatePlus20DaysInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 20);
        return c.getTime().getTime();
    }

    public Long setCurrentDatePlus120DaysInEpochFormat(){

        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 120);
        return c.getTime().getTime();
    }

    public Long setCurrentDatePlus5DaysInEpochFormat(){

//        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//        this.date = new Date();
//        this.calendar = Calendar.getInstance();
//        this.calendar.setTime(this.date);
//        this.calendar.add(5, 5);
//        this.date = this.calendar.getTime();
//        return this.simpleDateFormat.format(this.date);
        this.date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 5);
        return c.getTime().getTime();
    }

    public String getDateFromUnixTime(Long unixTime){

        Date date = new Date(unixTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+5:30")); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        System.out.println(formattedDate);
        return formattedDate;
    }

}
