package retail.app.util;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

/**
 * Created by Santhosh Kumar GA on 2016-09-22.
 */
public class PropHelper {
    public static String RandomNumber;
    public String Jenkinssource = "src//resources//config//stage_config.properties";
    public String localsource = "..//resources//config//stage_config.properties";
    public String source = Jenkinssource;
    public static String fullpath ="src//";
    public PropHelper() throws IOException {
        Random rand = new Random();
        //RandomNumber = rand.nextInt(1000) + 1;
        Date now = new Date();
        String dateString = now.toString();
        RandomNumber=DateFormat.getInstance().format(now);
        char[] myNameChars = RandomNumber.toCharArray();
        RandomNumber = String.valueOf(myNameChars);
        RandomNumber=RandomNumber.replaceAll("\\s+","_");
        System.out.println(RandomNumber);
        File directory = new File("./");
        System.out.println(directory.getAbsolutePath());
    }

    public static String getGADashBoard() throws IOException, JSONException {
        //String resourceName = "QA//config//searchmodel.json"; // could also be a constant
        File file = new File(fullpath + "resources//config//dashboard.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        JSONObject jObject  = new JSONObject(json);
        String DisplayName="G&A Dashboard:"+RandomNumber;
        jObject.put("DisplayName", DisplayName);
        String value = jObject.toString();
        fileInput.close();
        return value;
    }


    public  String getPropertyConf(String property) throws IOException {

        File file = new File(source);
        FileInputStream fileInput = new FileInputStream(file);
        Properties properties = new Properties();
        properties.load(fileInput);
        fileInput.close();

        //prop.load(PropHelper.class.getResourceAsStream("QA//config//stage_config.properties"));
        // get the property value and print it out
        String joynStageAnalyticshome = properties.getProperty(property);
        return joynStageAnalyticshome;
    }

    public String readFavourites() throws IOException, JSONException {
        File file = new File(fullpath + "resources/Navigation/favs.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read Price filters data

    public String priceFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/PriceFilter.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read 4G Phones filters data

    public String fourGPhonesFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/4Gphones.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read Dual Sim filters data

    public String dualSimsFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/DualSims.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read Ram filters data

    public String ramFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/RamFilter.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read Battery filters data

    public String batteryFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/BatteryFilters.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    // This method is used for read Screen Size filters data

    public String screenSizeFilters() throws IOException, JSONException {
        File file = new File(fullpath + "resources/BrowseByFeaturesFilters/ScreenSize.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String updateCartdata() throws IOException, JSONException{
        File file = new File(fullpath + "resources/OrderFlow/Updatecart.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String addtocart() throws IOException, JSONException{
        File file = new File(fullpath + "resources/OrderFlow/addToCart.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String login() throws IOException, JSONException{
        File file = new File(fullpath + "resources/LoginAndSignup/Login.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String signup() throws IOException, JSONException{
        File file = new File(fullpath + "resources/LoginAndSignup/Signup.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String getHintData() throws IOException, JSONException{
        File file = new File(fullpath + "resources/OffersFlow/getHintData.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

    public String placeBulkOrderRequest() throws IOException, JSONException{
        File file = new File(fullpath + "resources/OffersFlow/placeBulkOrder.json");
        FileInputStream fileInput = new FileInputStream(file);
        String json = IOUtils.toString(fileInput);
        fileInput.close();
        return json;
    }

}