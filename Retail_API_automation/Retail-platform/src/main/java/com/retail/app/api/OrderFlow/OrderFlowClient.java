package retail.app.api.OrderFlow;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.squareup.mimecraft.FormEncoding;
import com.squareup.okhttp.*;
import okhttp3.FormBody;
import retail.app.util.PropHelper;
import retail.app.util.HeaderFactory;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Santhosh.kumar@shotang.com on 20/06/2017
 */
public class OrderFlowClient {
    private static String testStage,updatecart,addToCart;
    public OrderFlowClient() throws IOException, JSONException {
        PropHelper helper=new PropHelper();
        testStage=helper.getPropertyConf("joyn_Stage_Analyticshome");
        updatecart=helper.updateCartdata();
        addToCart=helper.addtocart();

    }
    public static Response addToCart() throws Exception{
        String url = testStage + "/api/v1/cart/product/";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).body(addToCart).post(url);
        return response;
    }
    public static Response getcart() throws Exception{
        String url = testStage +"/api/v1/cart/v2/cart/";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).get(url);
        return response;
    }

    public static Response updateCart() throws Exception{
        String url = testStage + "/api/v1/cart/v2/cart/";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).body(updatecart).post(url);
        return response;
    }
    public static Response placeOrder() throws Exception{
        String url = testStage +"/api/v1/checkout/v2/success?pay_method=cod&wallet_amount=0.0";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).get(url);
        return response;
    }
    //https://staging.shotang.com
    public static Response getSOR_ID(String orderID, String token) throws Exception{
        String url = testStage +"/admin/index.php?route=sale/order&token=8c3079fa504d04e3c5b0a676d9cd15fc&filter_city_id=1&filter_parent_order_id="+orderID+"&access_token=846f6839a8d47055e4d7b3a827c72a4a7674dffe:517";
        System.out.println("URL :: "+url);
        HashMap<String, String> headers = HeaderFactory.defaultHeader();
        Response response= RestAssured.given().headers(headers).get(url);
        return response;
    }
    public static com.squareup.okhttp.Response ProcessOrder(String SORID, String token) throws Exception{
        String  url = testStage +"/admin/index.php?route=sale/order_products_update/processOrder&token=a4360028a1d79be741114317a6f373d3&access_token=846f6839a8d47055e4d7b3a827c72a4a7674dffe:517";
        System.out.println("URL :: "+url);
        OkHttpClient client = new OkHttpClient();


        RequestBody requestBody;
        MultipartBuilder mBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);
        mBuilder.addFormDataPart("order_id", "186608");
        requestBody = mBuilder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("cookie", "_gat=1; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168")
                .build();
        System.out.println("Request :: ");
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response;
    }

    public static com.squareup.okhttp.Response Process_Order_Selected(String orderID, String token) throws Exception{
        String url = testStage +"/admin/index.php?route=sale/order/processOrder&token=5a11f68748bb9cde49ebb04de2e60285&access_token=846f6839a8d47055e4d7b3a827c72a4a7674dffe:517";
        System.out.println("URL :: "+url);
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody;
        MultipartBuilder mBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);
        mBuilder.addFormDataPart("selected[]", "186630");
        requestBody = mBuilder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("cookie", "_gat=1; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168")
                .build();
        System.out.println("Request :: ");
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response;
    }
    public static com.squareup.okhttp.Response Process_Order_Picked(String orderID, String token) throws Exception{
        String url = testStage +"/admin/index.php?route=sale/order/shipped&token=5a11f68748bb9cde49ebb04de2e60285&access_token=846f6839a8d47055e4d7b3a827c72a4a7674dffe:517";
        System.out.println("URL :: "+url);
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody;
        MultipartBuilder mBuilder = new MultipartBuilder().type(MultipartBuilder.FORM);
        mBuilder.addFormDataPart("selected[]", "186630");
        requestBody = mBuilder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("cookie", "_gat=1; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168; currency=INR; PHPSESSID=h3ta68mdler33dv1f5g1kbsl52; _ga=GA1.2.1468183304.1497589794; _gid=GA1.2.775708801.1501053168")
                .build();
        System.out.println("Request :: ");
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response;
    }
}
