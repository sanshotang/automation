package com.retail.apptest.OrderFlow;

import com.jayway.restassured.response.Response;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import retail.app.api.OrderFlow.OrderFlowClient;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import java.util.ArrayList;

/**
 * Created by Santhosh.kumar@shotang.com on 20/06/2017
 */
public class OrderFlowTest {
    public static  String OrderID,SORID;
    @BeforeTest(groups = { "BuildCriteria", "Functional" })
    public void get_SessionID()throws Exception{
        System.out.println("initialization");
        OrderFlowClient order= new OrderFlowClient();
    }
    @Test(priority = 1,groups = { "BuildCriteria", "Functional" })

    public void test_add_To_cart() throws Exception {
        //Fetching response in JSON
        Response resp= OrderFlowClient.addToCart();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: Get Cart data");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj1 = (JSONObject) lev1.get("response");
        String result=obj1.get("message").toString();
        if (result.contains("success")) {
            Assert.assertTrue(true);
        }
        else {
            Assert.assertTrue(false, "Adding to cart failed");
        }

        JSONObject obj2 = (JSONObject) lev1.get("cart");
        JSONArray obj3 = (JSONArray) obj2.get("totals");
        for (int i1 = 0; i1 <= obj3.length() - 1; i1++) {
            JSONObject obj4 = obj3.getJSONObject(i1);
            int price= Integer.parseInt(obj4.get("value").toString());
            if(price>0){
                System.out.println("cart price"+price);
                flagFound=1;
            }
            else {
                System.out.println("cart price is invalid"+price);
                flagFound=0;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "Cart operations invalid");
        }


    }
    @Test(priority = 2,groups = { "BuildCriteria", "Functional" })

    public void test_get_cart() throws Exception {
        //Fetching response in JSON
        Response resp= OrderFlowClient.getcart();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: Get Cart data");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj1 = (JSONObject) lev1.get("data");
        JSONObject obj2 =(JSONObject) obj1.get("total");
        int price = Integer.parseInt(obj2.get("value").toString());
        if (price>0) {
            System.out.println("Cart price: "+price);
            Assert.assertTrue(true);
        }
        else {
            Assert.assertTrue(false, "Invalid cart price");
        }

        JSONArray obj3 = (JSONArray) obj1.get("products");
        for (int i1 = 0; i1 <= obj3.length() - 1; i1++) {
            JSONObject obj4 = obj3.getJSONObject(i1);
            int qty= Integer.parseInt(obj4.get("quantity").toString());
            if(qty>0){
                System.out.println("quantity: "+qty);
                flagFound=1;
            }
            else {
                System.out.println("quantity is invalid"+price);
                flagFound=0;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "Cart operations invalid");
        }

    }
    @Test(priority = 3,groups = { "BuildCriteria", "Functional" })
    public void test_Update_cart() throws Exception {
        //Fetching response in JSON
        Response resp= OrderFlowClient.updateCart();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: Update Cart data");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

    }
    @Test(priority = 4,groups = { "BuildCriteria", "Functional" })
    public void test_Place_Order() throws Exception {
        //Fetching response in JSON
        Response resp= OrderFlowClient.placeOrder();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: Place Order");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj1 = (JSONObject) lev1.get("data");
        String uor_id = obj1.get("uor_id").toString();
        String success= lev1.get("message").toString();
        if (uor_id.length()>0&& success.contains("success")) {
            flagFound=1;
            System.out.println("Order ID: "+uor_id);
            OrderID=uor_id;
            Assert.assertTrue(true);
        }
        else {
            Assert.assertTrue(false, "Order failed");
        }

        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "Checkout failed");
        }


    }
    @Test(priority = 4,groups = { "BuildCriteria", "Functional" })
    public void test_get_SORID() throws Exception {
        Response resp= OrderFlowClient.getSOR_ID(OrderID,"4051db499521a0c6ab1873772dab5eb2");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        System.out.println("Test case: Get SORID from Admin for given Order ID");
        Document doc = Jsoup.parse(value);
        Element table = doc.select("table").get(3); //select the first table.
        Elements rows = table.select("tr");
        System.out.println(table.tagName());
        for (int i = 1; i < rows.size(); i++) { //first row is the col names so skip it.
            Element row = rows.get(i);
            Elements cols = row.select("td");
            System.out.println(cols.get(1).text());
            if (cols.get(1).text().equals("143169")) {
                SORID=cols.get(2).text();
                System.out.println("SORid for order id:143169 is "+SORID);
                break;
            }
        }
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);
    }

    @Test(priority = 4,groups = { "BuildCriteria", "Functional" })
    public void Get_SORID_For_Order() throws Exception {
        Response resp= OrderFlowClient.getSOR_ID(OrderID,"4051db499521a0c6ab1873772dab5eb2");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        System.out.println("Test case: Process Order from admin");
        Document doc = Jsoup.parse(value);
        Element table = doc.select("table").get(3); //select the first table.
        Elements rows = table.select("tr");
        System.out.println(table.tagName());
        for (int i = 1; i < rows.size(); i++) { //first row is the col names so skip it.
            Element row = rows.get(i);
            Elements cols = row.select("td");
            System.out.println(cols.get(1).text());
            if (cols.get(1).text().equals("143169")) {
                SORID=cols.get(2).text();
                System.out.println("SORid for order id:143169 is "+SORID);
                break;
            }
        }
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);
    }

    @Test(priority = 5,groups = { "BuildCriteria", "Functional" })
    public void process_Order_Admin() throws Exception {
        //Order Processing
        int responseCode,actualResponseCode;
        com.squareup.okhttp.Response resp2= OrderFlowClient.Process_Order_Selected("186584","4051db499521a0c6ab1873772dab5eb2");
        String value = resp2.toString();
        System.out.println(value);

        resp2= OrderFlowClient.Process_Order_Picked("186584","4051db499521a0c6ab1873772dab5eb2");
        value = resp2.toString();
        System.out.println(value);
        /*responseCode=resp2.code();
        actualResponseCode=200;
        String message=resp2.message();
        Assert.assertEquals(responseCode,actualResponseCode,message);*/

    }


}
