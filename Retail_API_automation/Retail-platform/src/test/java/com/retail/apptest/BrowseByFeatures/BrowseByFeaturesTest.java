package com.retail.apptest.BrowseByFeatures;
import retail.app.api.BrowseByFeatures.BrowseByFeaturesApis;
import com.jayway.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
* Created by Srikanth.k@shotang.com on 06/07/2017
*/
public class BrowseByFeaturesTest {

   @BeforeTest(groups = { "BuildCriteria", "Functional" })
   public void get_SessionID()throws Exception{
       BrowseByFeaturesApis browsebyfeature = new BrowseByFeaturesApis();

   }


    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_products_for_browse_by_features_Dual_SIM() throws Exception {

        //Fetching response in JSON
        String entityArray[]= {"1" };
        String FilterName[]= {"Dual SIM","Ram","Battery", "Size","3G/4G"};
        String description=null;
        for (int i=0; i<entityArray.length; i++)

        {
            int flagFound=0;
            Response resp= BrowseByFeaturesApis.getProductsForBrowseByFeatures(entityArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            System.out.println("Test case: test get products for Browse by Features entity id = " + entityArray[i] + "\n");
            System.out.println(value);
            int responseCode,actualResponseCode;
            responseCode=resp.statusCode();
            actualResponseCode=200;
            String message=resp.sessionId();
            Assert.assertEquals(responseCode,actualResponseCode,message);

            //Validations
            value = '[' + value + ']';
            JSONArray jsonresponce= new JSONArray(value);
            JSONObject lev1=  jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            //System.out.println(obj);
            JSONArray obj1= (JSONArray) obj.get("products");
            //  System.out.println(obj1);
            for(int i1=0;i1<=obj1.length()-1;i1++)
            {
                description =obj1.getJSONObject(i1).get("description").toString();
                if(description.contains("Dual SIM")){
                    System.out.println("Dual Sim Description:"+i1+" nos "+description+"\n");
                    flagFound=1;
                }
            }
            if(flagFound==1){
                Assert.assertTrue(true);
            }
            else{
                Assert.assertTrue(false,"Dual Sim Description Not found");
            }

        }
    }

    // validate browse by features with dual sim data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void validate_browse_by_feature_with_dual_sim_data_() throws Exception {

        //Fetching response in JSON
        String entityArray[]= {"1" };
        String FilterName[]= {"Dual SIM","Ram","Battery", "Size","3G/4G"};
        String description=null;
        for (int i=0; i<entityArray.length; i++)

        {
            int flagFound=0;
            Response resp= BrowseByFeaturesApis.getProductsForBrowseByFeatures(entityArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            System.out.println("Test case: test get products for Browse by Features entity id = " + entityArray[i] + "\n");
            System.out.println(value);
            int responseCode,actualResponseCode;
            responseCode=resp.statusCode();
            actualResponseCode=200;
            String message=resp.sessionId();
            Assert.assertEquals(responseCode,actualResponseCode,message);

            //Validations
            value = '[' + value + ']';
            JSONArray jsonresponce= new JSONArray(value);
            JSONObject lev1=  jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            //System.out.println(obj);
            JSONArray obj1= (JSONArray) obj.get("products");
            //  System.out.println(obj1);
            for(int i1=0;i1<=obj1.length()-1;i1++)
            {
                JSONArray obj2=  (JSONArray)obj1.getJSONObject(i1).get("variants");

                description =obj1.getJSONObject(i1).get("description").toString();
                if(description.contains("Dual SIM")){
                    System.out.println("Dual Sim Description:"+i1+" nos "+description+"\n");
                    flagFound=1;
                    for(int i2=0;i2<obj2.length();i2++){
                        String optValid =obj2.getJSONObject(i2).get("option_1_value_id").toString();
                        System.out.println("option val id:"+i2+" nos "+optValid+"\n");
                        JSONArray obj3=  (JSONArray)obj2.getJSONObject(i2).get("slabs");
                        for(int i3=0;i3<obj3.length();i3++){
                            String slab =obj3.getJSONObject(i3).get("slab_name").toString();
                            System.out.println("slab name details:"+i3+" nos "+slab+"\n");
                        }
                    }
                }
            }
            if(flagFound==1){
                Assert.assertTrue(true);
            }
            else{
                Assert.assertTrue(false,"Dual Sim Description Not found");
            }


        }
    }

    // Test case is for Price Filter Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_price_filter() throws Exception {
        //Fetching response in JSON
        int price;
        Response resp = BrowseByFeaturesApis.applyPriceFilter();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_price_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        int flagFound=0;
        //Validations
        JSONArray jsonresponce= new JSONArray(value);
        JSONObject lev1=  jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1= (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for(int i1=0;i1<=obj1.length()-1;i1++)
        {
            JSONArray obj2= (JSONArray) obj1.getJSONObject(i1).get("variants");
            for(int i2=0;i2<=obj2.length()-1;i2++)
            {
                JSONArray obj3= (JSONArray) obj2.getJSONObject(i2).get("slabs");
                for(int i3=0;i3<=obj3.length()-1;i3++) {
                    price = Integer.parseInt(obj3.getJSONObject(i3).get("final_price").toString());

                    if (price>=6000 && price<=8000) {
                        System.out.println("Price range:" + i1 + " nos " + price + "\n");
                        flagFound = 1;
                    }
                    else{
                        flagFound=0;
                        System.out.println("Price out of range:" + i1 + " nos " + price + "\n");
                        break;
                    }
                }
            }
        }
        if(flagFound==1){
            Assert.assertTrue(true);
        }
        else{
            Assert.assertTrue(false,"out of price range data found");
        }

    }

    // Test case is for 4G Phones Filter Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_fourGPhone_filter() throws Exception {
        //Fetching response in JSON
        Response resp = BrowseByFeaturesApis.applyFourGPhonesFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_fourGPhone_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // validate Battery range data on browse by feature section.

    @Test(groups = { "BuildCriteria", "Functional" })
    public void validate_2000mah_Battery_data_under_browse_by_feature_section() throws Exception {

            String description=null;
            int flagFound=0;
            Response resp = BrowseByFeaturesApis.applyBatteryFilters();
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test_apply_battery_filter" + "\n");
            System.out.println(jsonResponse);
            int responseCode, actualResponseCode;
            responseCode = resp.statusCode();
            actualResponseCode = 200;
            String message = resp.sessionId();
            Assert.assertEquals(responseCode, actualResponseCode, message);

            //Validations
            JSONArray jsonresponce= new JSONArray(value);
            JSONObject lev1=  jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            //System.out.println(obj);
            JSONArray obj1= (JSONArray) obj.get("products");
            //  System.out.println(obj1);
            for(int i1=0;i1<=obj1.length()-1;i1++)
            {
                description =obj1.getJSONObject(i1).get("description").toString();
                if(description.contains("2000")){
                    System.out.println(" Description:"+i1+" nos "+description+"\n");
                    flagFound=1;
                }
            }
            if(flagFound==1){
                Assert.assertTrue(true);
            }
            else{
                Assert.assertTrue(false,"3000mAh Battery Description Not found");
            }

        }

    // validate 5 inch Screen Size data on browse by feature section.

    @Test(groups = { "BuildCriteria", "Functional" })
    public void validate_5_inch_screen_size_under_browse_by_feature_section() throws Exception {

        String description=null;
        int flagFound=0;
        Response resp = BrowseByFeaturesApis.applyScreenSizeFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_screen_size_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations
        JSONArray jsonresponce= new JSONArray(value);
        JSONObject lev1=  jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1= (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for(int i1=0;i1<=obj1.length()-1;i1++)
        {
            description =obj1.getJSONObject(i1).get("description").toString();
            if(description.contains("5")){
                System.out.println(" Description: "+i1+" --> "+description+"\n");
                flagFound=1;
            }
        }
        if(flagFound==1){
            Assert.assertTrue(true);
        }
        else{
            Assert.assertTrue(false," 5.0 inches Description Not found");
        }

    }


    // Test case is for Dual Sims Filters Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_dual_sims_filter() throws Exception {
        //Fetching response in JSON
        Response resp = BrowseByFeaturesApis.applyDualSimsFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_dual_sims_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // Test case is for Ram Filters Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_ram_filter() throws Exception {
        //Fetching response in JSON
        Response resp = BrowseByFeaturesApis.applyRamFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_ram_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // Test case is for Battery Filters Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_battery_filter() throws Exception {
        //Fetching response in JSON
        Response resp = BrowseByFeaturesApis.applyBatteryFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_battery_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

// Test case is for Screen Size Filters Data

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_apply_screen_size_filter() throws Exception {
        //Fetching response in JSON
        Response resp = BrowseByFeaturesApis.applyScreenSizeFilters();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_apply_screen_size_filter" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }
}