package com.retail.apptest.AutoMobilesTest;

import com.jayway.restassured.response.Response;
import org.json.JSONArray;
import org.testng.Assert;
import org.json.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import retail.app.api.AutoMobiles.CarsAndBikesClients;

/**
* Created by Srikanth.k@shotang.com on 21/08/2017
*/

public class CarsAndBikesTest {

    @BeforeTest(groups = {"BuildCriteria", "Functional"})
    public void get_SessionID() throws Exception {
        CarsAndBikesClients CarsAndBikes = new CarsAndBikesClients();
    }

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_Automobiles_Categories() throws Exception {

        //Fetching response in JSON
        String entityArray[] = {"Bikes", "Cars"};
        Response resp = CarsAndBikesClients.getCategories();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_Automobiles_Categories");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONArray obj1 = (JSONArray) lev1.get("data");
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONObject obj2 = obj1.getJSONObject(i1);
            String name = obj2.get("display_name").toString();
            String id = obj2.get("id").toString();
            if (name.equals(entityArray[i1])) {
                System.out.println("Category id :" + id);
                System.out.println("Category found:" + name);
                flagFound = 1;
            } else {
                System.out.println("Category expected" + entityArray[i1] + " found:" + name);
                flagFound = 0;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "Categories not matching");
        }
    }

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_Automobiles_advanced_filters_data() throws Exception {
        //Fetching response in JSON
        Response resp = CarsAndBikesClients.getAutomobilesAdvancedFilterData();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_Automobiles_advanced_filters_data" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_Automobiles_cards_details() throws Exception {
        //Fetching response in JSON
        String catArray[] = {"172", "173"};
        for (int i = 0; i < catArray.length; i++) {
            Response resp = CarsAndBikesClients.getAutomobilesCardsDetails(catArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test get category for catgeory id = " + catArray[i] + "\n");
            System.out.println(jsonResponse);
            int responseCode, actualResponseCode;
            responseCode = resp.statusCode();
            actualResponseCode = 200;
            String message = resp.sessionId();
            Assert.assertEquals(responseCode, actualResponseCode, message);

            //Validation
            int flagFound = 0;
            JSONArray jsonresponce = new JSONArray(value);
            JSONObject lev1 = jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            JSONArray obj1 = (JSONArray) obj.get("cards");
            for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
                JSONObject obj2 = obj1.getJSONObject(i1);
                String name = obj2.get("name").toString();
                String id = obj2.get("id").toString();
                String category_id = obj2.get("category_id").toString();
                String city_id = obj2.get("city_id").toString();
                String is_active = obj2.get("is_active").toString();
                if (is_active.contains("1")) {
                    System.out.println("Parent Category Name :  " + category_id + "\n");
                    System.out.println("Category Name :  " + name + "\n");
                    System.out.println("Sub Category Id :  " + id + "\n");
                    flagFound = 1;
                }
                if (flagFound == 1) {
                    Assert.assertTrue(true);
                } else {
                    Assert.assertTrue(false, "Category Name and Ids are Not found");
                }
            }
        }
    }

    
    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_Automobiles_cars_and_bikes_cards_products() throws Exception {
        //Fetching response in JSON
        String entityArray[] = {"177","178", "183", "185", "186"};
        for (int i = 0; i < entityArray.length; i++) {
            Response resp = CarsAndBikesClients.getAutomobilesCarsAndBikesCardsProductDetails(entityArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test_Automobiles_cars_and_bikes_cards_products = " + entityArray[i] + "\n");
            System.out.println(jsonResponse);
            int responseCode, actualResponseCode;
            responseCode = resp.statusCode();
            actualResponseCode = 200;
            String message = resp.sessionId();
            Assert.assertEquals(responseCode, actualResponseCode, message);
        }
    }
}