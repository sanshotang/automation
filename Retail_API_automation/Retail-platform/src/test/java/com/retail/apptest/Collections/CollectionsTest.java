package com.retail.apptest.Collections;

import org.json.JSONObject;
import retail.app.api.Collections.CollectionsApis;
import com.jayway.restassured.response.Response;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
* Created by Srikanth.k@shotang.com on 03/07/2017
*/
public class CollectionsTest {

    @BeforeTest(groups = {"BuildCriteria", "Functional"})
    public void get_SessionID() throws Exception {
        CollectionsApis collection = new CollectionsApis();

    }


    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_products_for_collections_BEST_SELLING() throws Exception {

        //Fetching response in JSON
        String entityArray[] = {"1", "2", "17", "19"};
        Response resp = CollectionsApis.getProductsForCollections("1");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test get products for collections entity id = " + "1 - BEST_SELLING" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations

        int flagFound = 0;
        //Validations
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1 = (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONArray obj2 = (JSONArray) obj1.getJSONObject(i1).get("variants");
            for (int i2 = 0; i2 <= obj2.length() - 1; i2++) {
                JSONArray obj3 = (JSONArray) obj2.getJSONObject(i2).get("slabs");
                for (int i3 = 0; i3 <= obj3.length() - 1; i3++) {
                    String price = obj3.getJSONObject(i3).get("final_price").toString();

                    if (price.length() >= 1) {
                        System.out.println("Price range:" + i1 + " nos " + price + "\n");
                        flagFound = 1;
                    } else {
                        flagFound = 0;
                        System.out.println("Price out of range:" + i1 + " nos " + price + "\n");
                        break;
                    }
                }
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "out of price range data found");
        }


    }

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_products_for_collections_NEWLY_LAUNCHED() throws Exception {

        String entityArray[] = {"1", "2", "17", "19"};
        Response resp = CollectionsApis.getProductsForCollections("2");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test get products for collections entity id = " + "2 - NEWLY_LAUNCHED" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1 = (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONObject obj2 = obj1.getJSONObject(i1);
            String name = obj2.get("name").toString();
            String description = obj2.get("description").toString();
            System.out.println("Product name found :" + i1 + name);
            System.out.println("Product description found :" + i1 + description);
        }
    }


    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_products_for_collections_HIGH_MARGIN_FEATURE_PHONES() throws Exception {

        String entityArray[] = {"1", "2", "17", "19"};
        Response resp = CollectionsApis.getProductsForCollections("17");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test get products for collections entity id = " + "1 - HIGH_MARGIN_FEATURE_PHONES" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations

/*        int flagFound = 0;
        //Validations
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1 = (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONArray obj2 = (JSONArray) obj1.getJSONObject(i1).get("variants");
            for (int i2 = 0; i2 <= obj2.length() - 1; i2++) {
                JSONArray obj3 = (JSONArray) obj2.getJSONObject(i2).get("slabs");
                for (int i3 = 0; i3 <= obj3.length() - 1; i3++) {
                    String price = obj3.getJSONObject(i3).get("final_price").toString();

                    if (price.length() >= 1) {
                        System.out.println("Price range:" + i1 + " nos " + price + "\n");
                        flagFound = 1;
                    } else {
                        flagFound = 0;
                        System.out.println("Price out of range:" + i1 + " nos " + price + "\n");
                        break;
                    }
                }
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "out of price range data found");
        }*/
    }

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_products_for_collections_MULTI_TIER_PRODUCTS() throws Exception {

        String entityArray[] = {"1", "2", "17", "19"};
        Response resp = CollectionsApis.getProductsForCollections("19");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test get products for collections entity id = " + "1 - MULTI_TIER_PRODUCTS" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

        //Validations

        int flagFound = 0;
        //Validations
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        //System.out.println(obj);
        JSONArray obj1 = (JSONArray) obj.get("products");
        //  System.out.println(obj1);
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONArray obj2 = (JSONArray) obj1.getJSONObject(i1).get("variants");
            for (int i2 = 0; i2 <= obj2.length() - 1; i2++) {
                JSONArray obj3 = (JSONArray) obj2.getJSONObject(i2).get("slabs");
                for (int i3 = 0; i3 <= obj3.length() - 1; i3++) {
                    String price = obj3.getJSONObject(i3).get("final_price").toString();

                    if (price.length() >= 1) {
                        System.out.println("Price range:" + i1 + " nos " + price + "\n");
                        flagFound = 1;
                    } else {
                        flagFound = 0;
                        System.out.println("Price out of range:" + i1 + " nos " + price + "\n");
                        break;
                    }
                }
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "out of price range data found");
        }

    }
}