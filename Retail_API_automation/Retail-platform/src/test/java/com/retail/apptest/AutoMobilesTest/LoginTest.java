package com.retail.apptest.AutoMobilesTest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import retail.app.api.AutoMobiles.LoginApi;

/**
* Created by Srikanth.k@shotang.com on 17/08/2017
*/
public class LoginTest {

    @BeforeTest(groups = {"BuildCriteria", "Functional"})
    public void get_SessionID() throws Exception {
        LoginApi Login = new LoginApi();
    }

    // Test case is used for verifying Retailer Login flow

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_verify_auto_mobile_retailer_login_flow() throws Exception {
        //Fetching response in JSON
        String message = null;

        com.squareup.okhttp.Response resp = LoginApi.verifyAutoMobilesLoginFlow("testautomobile@shotang.com", "testpassword");
        byte ptext[] = resp.body().string().getBytes();
        String value = new String(ptext, "UTF-8");
        value = '[' + value + ']';
        System.out.println("Test case: test_verify_retailer_login_flow" + "\n");
        System.out.println(value);
        int responseCode, actualResponseCode;
        actualResponseCode = 200;

        // validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject obj = new JSONObject(jsonresponce.getJSONObject(0).get("response").toString());
        message =obj.get("message").toString();
        if (message.contains("The login was successfull")) {
            System.out.println("The login was successfull");
            flagFound = 1;
        }
            if (flagFound == 1) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false, "Invalid User Name or Password");
            }
        }
}