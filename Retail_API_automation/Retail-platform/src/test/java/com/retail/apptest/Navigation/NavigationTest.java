package com.retail.apptest.Navigation;

import com.jayway.restassured.response.Response;
import org.json.JSONObject;
import retail.app.api.Navigation.NavigationClient;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by Santhosh.kumar@shotang.com on 20/06/2017
 */
public class NavigationTest {

    @BeforeTest(groups = { "BuildCriteria", "Functional" })
    public void get_SessionID()throws Exception{
        NavigationClient navi=new NavigationClient();

    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_Categories() throws Exception {

        //Fetching response in JSON
        String entityArray[] = {"Mobiles", "Accessories", "Tablets"};
        Response resp= NavigationClient.getCategories();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_Categories");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONArray obj1 = (JSONArray)lev1.get("data");
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            JSONObject obj2 = obj1.getJSONObject(i1);
            String catid=obj2.get("id").toString();
            String name=obj2.get("display_name").toString();
            if(name.equals(entityArray[i1])){
                System.out.println("Category ID found:"+catid);
                System.out.println("Category name found:"+name + "\n" );
                flagFound=1;
            }
            else {
                System.out.println("Category ID expected"+ entityArray[i1]+" found:"+catid);
                System.out.println("Category name expected"+ entityArray[i1]+" found:"+name);
                flagFound=0;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "Categories not matching");
        }
    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_products_For_Accessories_SubCategory_Power_Banks() throws Exception {

        //Fetching response in JSON
        Response resp= NavigationClient.getProductsForAccessoriesSubCategory();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_products_For_Accessories_SubCategory_Power_Banks" + "\n");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        JSONArray obj1 = (JSONArray) obj.get("products");
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            String name = obj1.getJSONObject(i1).get("name").toString();
            if (name.contains("Power Bank")) {
                System.out.println("Power Bank Name:" + i1 + "  " + name + "\n");
                flagFound = 1;
            } else {
                flagFound = 0;
                System.out.println("Power Bank Name:" + i1 + "  " + name + "\n");
                break;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "There is No Power Bank data found");
        }
    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_Cards_For_ALL_Category() throws Exception {

        //Fetching response in JSON
        String catArray[]= {"59","121","97" };
        for (int i=0; i<catArray.length; i++)

        {
            Response resp= NavigationClient.getCardsForMobileCategory(catArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value  + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test get category for catgeory id = " + catArray[i] + "\n");
            System.out.println(jsonResponse);
            int responseCode,actualResponseCode;
            responseCode=resp.statusCode();
            actualResponseCode=200;
            String message=resp.sessionId();
            Assert.assertEquals(responseCode,actualResponseCode,message);

            //Validations
            int flagFound = 0;
            JSONArray jsonresponce = new JSONArray(value);
            JSONObject lev1 = jsonresponce.getJSONObject(0);
            JSONObject obj1 = (JSONObject) lev1.get("data");
            JSONObject obj2 = (JSONObject) obj1.get("all");
            JSONArray obj3 = (JSONArray) obj2.get("data");
            for (int i1 = 0; i1 <= obj3.length() - 1; i1++) {
                JSONObject obj4 = obj3.getJSONObject(i1);
                String name=obj4.get("name").toString();
                if(name.length()>0){
                    System.out.println("brand found:"+name);
                    flagFound=1;
                }
                else {
                    System.out.println("No brand found"+name);
                    flagFound=0;
                }
            }
            if (flagFound == 1) {
                Assert.assertTrue(true);
            } else {
                Assert.assertTrue(false, "Categories not matching");
            }
        }
    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_products_For_Nokia_Brand() throws Exception {

        //Fetching response in JSON
        Response resp= NavigationClient.getProductsForNokiaBrand();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_products_For_Nokia_Brand" + "\n");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        JSONArray obj1 = (JSONArray) obj.get("products");
        for (int i1 = 0; i1 <= obj1.length() - 1; i1++) {
            String name = obj1.getJSONObject(i1).get("name").toString();
            if (name.contains("Nokia")) {
                System.out.println("Nokia Product Name:" + i1 + "  " + name + "\n");
                flagFound = 1;
            } else {
                flagFound = 0;
                System.out.println("Nokia Product Name:" + i1 + "  " + name + "\n");
                break;
            }
        }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "There is No Nokia product data found");
        }
    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_Nokia105DS_product_details() throws Exception {

        //Fetching response in JSON
        Response resp= NavigationClient.getProductDetails("2138");
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_Nokia105DS_product_details" + "\n");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        //Validations
        int flagFound = 0;
        JSONArray jsonresponce = new JSONArray(value);
        JSONObject lev1 = jsonresponce.getJSONObject(0);
        JSONObject obj = new JSONObject(lev1.get("data").toString());
        String name = obj.get("name").toString();
        String pid = obj.get("product_id").toString();
        if (name.contains("Nokia 105 Ds")) {
            System.out.println("Product Name:" + "  " + name + "\n");
            System.out.println("Product Id:" + "  " + pid + "\n");
            flagFound = 1;
        } else {
                flagFound = 0;
                System.out.println("Product Name:" + "  " + name + "\n");
                System.out.println("Product Id:" + "  " + pid + "\n");
                }
        if (flagFound == 1) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false, "There is No Nokia 105 DS product data found");
            }
    }

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_Add_Favourite_and_Delete() throws Exception {

        //Fetching response in JSON
        String entityID,catID;
        entityID="25";
        catID="59";
        Response resp= NavigationClient.postFavourite(entityID,catID);
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_Add_Favourite_and_Delete" + "\n");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);

        // Delete same favourite from user
        resp= NavigationClient.deleteFavs(entityID,catID);
        ptext = resp.asString().getBytes();
        value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        jsonResponse = new JSONArray(value);
        System.out.println(jsonResponse);
        responseCode=resp.statusCode();
        actualResponseCode=200;
        message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);
    }
}