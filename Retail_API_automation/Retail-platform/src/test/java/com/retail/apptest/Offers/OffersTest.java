package com.retail.apptest.Offers;

import org.json.JSONObject;
import retail.app.api.Offers.OffersApis;
import com.jayway.restassured.response.Response;
import org.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
* Created by Srikanth.k@shotang.com on 11/07/2017
*/
public class OffersTest {

   @BeforeTest(groups = { "BuildCriteria", "Functional" })
   public void get_SessionID()throws Exception{
       OffersApis Offer =new OffersApis();

   }

    // Test case is used for getting the shotang cashback offers

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_products_For_Offer_Type_As_Cashback_offers() throws Exception {

        //Fetching response in JSON

        String catArray[] = {"59", "167", "121", "97"} ;

        for (int i=0; i<catArray.length; i++){

            Response resp= OffersApis.getProductsForOfferTypeAsCashback(catArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value  + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test_get_products_For_Offer_Type_As_Cashback_offers" + "\n");
            System.out.println(jsonResponse);
            int responseCode,actualResponseCode;
            responseCode=resp.statusCode();
            actualResponseCode=200;
            String message=resp.sessionId();
            Assert.assertEquals(responseCode,actualResponseCode,message);

            //Validations
            int flagFound=1;
            JSONArray jsonresponce= new JSONArray(value);
            JSONObject lev1=  jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            //System.out.println(obj);
            JSONArray obj1= (JSONArray) obj.get("products");
            //  System.out.println(obj1);
            for(int i1=0;i1<=obj1.length()-1;i1++)
            {
                String description =obj1.getJSONObject(i1).get("description").toString();
                if(description.contains("Dual SIM")){
                    System.out.println("Dual Sim Description:"+i1+" nos "+description+"\n");
                    flagFound=1;
                }
            }
            if(flagFound==1){
                Assert.assertTrue(true);
            }
            else{
                Assert.assertTrue(false,"Dual Sim Description Not found");
            }

        }

    }


    // Test case is used for getting the Bundle offers

    @Test(groups = { "BuildCriteria", "Functional" })
    public void test_get_products_For_Offer_Type_As_Bundle_offers() throws Exception {

        //Fetching response in JSON

        String catArray[] = {"59", "167", "121", "97"} ;

        for (int i=0; i<catArray.length; i++){

            Response resp= OffersApis.getProductsForOfferTypeAsBundleOffer(catArray[i]);
            byte ptext[] = resp.asString().getBytes();
            String value = new String(ptext, "UTF-8");
            //System.out.println(value);
            value = '[' + value  + ']';
            JSONArray jsonResponse = new JSONArray(value);
            System.out.println("Test case: test_get_products_For_Offer_Type_As_Bundle_offers" + "\n");
            System.out.println(jsonResponse);
            int responseCode,actualResponseCode;
            responseCode=resp.statusCode();
            actualResponseCode=200;
            String message=resp.sessionId();
            Assert.assertEquals(responseCode,actualResponseCode,message);

            //Validations
            int flagFound=1;
            JSONArray jsonresponce= new JSONArray(value);
            JSONObject lev1=  jsonresponce.getJSONObject(0);
            JSONObject obj = new JSONObject(lev1.get("data").toString());
            //System.out.println(obj);
            JSONArray obj1= (JSONArray) obj.get("products");
            //  System.out.println(obj1);
            for(int i1=0;i1<=obj1.length()-1;i1++)
            {
                String description =obj1.getJSONObject(i1).get("description").toString();
                if(description.contains("Dual SIM")){
                    System.out.println("Dual Sim Description:"+i1+" nos "+description+"\n");
                    flagFound=1;
                }
            }
            if(flagFound==1){
                Assert.assertTrue(true);
            }
            else{
                Assert.assertTrue(false,"Dual Sim Description Not found");
            }
        }

    }

    // Test case is used for getting the group deal

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_group_deals_details() throws Exception {
        //Fetching response in JSON
        Response resp = OffersApis.getGroupDeals();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_group_deals_details" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);

    }

    // Test case is used for getting the Bulk Order request Card

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_bulk_orders_request_card_details() throws Exception {
        //Fetching response in JSON
        Response resp = OffersApis.getBulkOrderCard();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_bulk_order_card_details" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }


    // Test case is used for getting the Bulk Order request Terms and Condition Page

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_bulk_orders_request_terms_page() throws Exception {
        //Fetching response in JSON
        Response resp = OffersApis.getBulkOrdersTermsPage();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_bulk_orders_terms_page" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // Test case is used for getting the Bulk Order request Misc data

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_bulk_orders_request_misc_data() throws Exception {
        //Fetching response in JSON
        Response resp = OffersApis.getBulkOrdersMiscData();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_bulk_orders_request_misc_data" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // Test case is used for getting the Bulk Order request Nokia 105 Product data

    @Test(groups = {"BuildCriteria", "Functional"})
    public void test_get_bulk_orders_request_nokia105_product_data() throws Exception {
        //Fetching response in JSON
        Response resp = OffersApis.getBulkOrdersNokia105ProductData();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_bulk_orders_request_nokia105_product_data" + "\n");
        System.out.println(jsonResponse);
        int responseCode, actualResponseCode;
        responseCode = resp.statusCode();
        actualResponseCode = 200;
        String message = resp.sessionId();
        Assert.assertEquals(responseCode, actualResponseCode, message);
    }

    // Test case is used for getting the Bulk Order request Product Hint data

    @Test(priority = 3,groups = { "BuildCriteria", "Functional" })
    public void test_get_product_hint_data_like_best_price_and_min_qty() throws Exception {
        //Fetching response in JSON
        Response resp= OffersApis.getHintData();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_get_product_hint_data_like_best_price_and_min_qty");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);
    }

    // Test case is used for placing the Bulk Order Request

    @Test(priority = 3,groups = { "BuildCriteria", "Functional" })
    public void test_place_bulk_order_request() throws Exception {
        //Fetching response in JSON
        Response resp= OffersApis.placeBulkOrderRequest();
        byte ptext[] = resp.asString().getBytes();
        String value = new String(ptext, "UTF-8");
        //System.out.println(value);
        value = '[' + value  + ']';
        JSONArray jsonResponse = new JSONArray(value);
        System.out.println("Test case: test_place_bulk_order_request");
        System.out.println(jsonResponse);
        int responseCode,actualResponseCode;
        responseCode=resp.statusCode();
        actualResponseCode=200;
        String message=resp.sessionId();
        Assert.assertEquals(responseCode,actualResponseCode,message);
    }

}